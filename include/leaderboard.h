#pragma once

#include <string>
#include <vector>
#include <ctime>

struct Players;

struct Player
{
    std::string name;
    int high_score_value = 0;
    int high_score_timestamp = time(NULL);
    Players* players_obj;
    int index_in_players_vec;
    Player (Players* players_obj, std::string name);
    void OnAchievedScore (int score);
    std::string toString();
};

struct Players
{
    std::vector<Player*> players_vec;
    void PrintAllPlayers ();
    void AddPlayer (Player* player);
    void RemovePlayer (Player* player);
    void UpdatePlayer (Player* player);
};
