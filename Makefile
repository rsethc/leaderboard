CFLAGS = -g -I include

OBJS = $(patsubst %.cpp,%.o,$(shell find src -iname '*.cpp'))

%.o: %.cpp
	g++ -c $^ -o $@ $(CFLAGS)

bin/leaderboard: $(OBJS)
	mkdir -p bin
	g++ $(OBJS) -o bin/leaderboard

default: bin/leaderboard
