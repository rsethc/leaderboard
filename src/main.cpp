#include "leaderboard.h"

#include <iostream>
using namespace std;

int main ()
{
    Players players;
    players.PrintAllPlayers();

    Player* Jimmy = new Player {&players, "Jimmy"};
    Player* Johnny = new Player {&players, "Johnny"};

    players.AddPlayer(Jimmy);
    players.PrintAllPlayers();

    players.AddPlayer(Johnny);
    players.PrintAllPlayers();

    players.RemovePlayer(Jimmy);
    players.PrintAllPlayers();

    players.AddPlayer(Jimmy);
    players.PrintAllPlayers();

    players.UpdatePlayer(Jimmy);
    players.PrintAllPlayers();

    Jimmy->OnAchievedScore(100);
    players.PrintAllPlayers();

    Johnny->OnAchievedScore(150);
    players.PrintAllPlayers();

    Jimmy->OnAchievedScore(160);
    players.PrintAllPlayers();

    return 0;
}
