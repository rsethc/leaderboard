#include "leaderboard.h"

#include <string>
#include <iostream>
#include <sstream>

using namespace std;

Player::Player (Players* players_obj, string name)
    : players_obj(players_obj)
    , name(name)
{}
void Player::OnAchievedScore (int score)
{
    if (score > high_score_value)
    {
        high_score_value = score;
        high_score_timestamp = time(NULL);
    }
    players_obj->UpdatePlayer(this);
}

void Players::PrintAllPlayers ()
{
    cout << "Players:" << endl;
    for (Player* player : players_vec)
    {
        cout << player->toString();
    }
}
void Players::AddPlayer (Player* player)
{
    cout << "Adding Player: "<< player->toString();
    player->index_in_players_vec = players_vec.size();
    players_vec.push_back(player);
}
void Players::RemovePlayer (Player* player)
{
    cout << "Removing player: "  << player->toString();
    players_vec[player->index_in_players_vec] = players_vec[players_vec.size() - 1];
    players_vec[player->index_in_players_vec]->index_in_players_vec = player->index_in_players_vec;
    players_vec.pop_back();
}
void Players::UpdatePlayer (Player* player)
{
    cout << "Changed player: " << player->toString();
    RemovePlayer(player);
    AddPlayer(player);
}
string Player::toString()
{
    stringstream ss;
    ss << "Name: " << name << " | ";
    ss << "High Score: " << high_score_value << " | ";
    ss << "\n";
    return ss.str();
}